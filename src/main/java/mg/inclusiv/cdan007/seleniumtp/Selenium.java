/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan007.seleniumtp;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author marcr
 */
public class Selenium {
    public static void main(String[] args) throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\marcr\\Downloads\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe");

        // manao max size ny chrome
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");

        WebDriver driver = new ChromeDriver(options);
        driver.get("https://www.zoma.mg");
        String itemToSearch = "Chocolat" ;
        //maka ny searcbox dia manisy data ao anatiny
        driver.findElement(By.name("s")).sendKeys(itemToSearch);
        driver.findElement(By.xpath("//*[@id=\"searchbox\"]/button")).click();

        //misafidy ilay element voalohany
        driver.findElement(By.cssSelector("#js-product-list > div > div:nth-child(1) > article > div.img_block")).click();

        //manampy isa 1 ilay element
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[1]/div/span[3]/button[1]")).click();
        //mapiditra anaty panier
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[2]/button")).click();

        //attente de 5s pour que le modal s'ouvre et qu'on puisse continuer
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //makany am commander
        driver.findElement(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/a[2]")).click();
        
        WebElement inputNumber = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[1]/div[1]/div[2]/ul/li/div/div[3]/div/div[2]/div/div[1]/div/input"));
        
        int nombreItem = Integer.parseInt(inputNumber.getAttribute("value"));
        System.out.println(nombreItem);
        
        //condition contains 
        if (nombreItem == 2) {
            //micommande
            driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div[1]/div[2]/div/a")).click();
           
            // Manao capture d'écran
            File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String destinationPath = "C:\\Users\\marcr\\Desktop\\screenshot.png";

            try {
                // Copiez la capture d'écran vers l'emplacement de destination
                Path destination = Paths.get(destinationPath);
                Files.copy(screenshotFile.toPath(), destination, StandardCopyOption.REPLACE_EXISTING);
                System.out.println("Capture d'écran enregistrée avec succès à l'emplacement : " + destinationPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        else {
            driver.quit();
            System.out.println("Tsy nety ilay incrémentaion item na tsy affiché ilay item");
        }
      
    }
}
